package pl.edu.agh.informatyka.bo.bee;

import static pl.edu.agh.informatyka.bo.bee.BeeAlgorithmParams.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pl.edu.agh.informatyka.bo.flowshop.ISolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.SolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

public class BeeSolver implements ISolver {

	private int n, m, e, nep, nsp, ngh, imax;
	private TestInstance testInstance;
	private ExecutorService service;

	public BeeSolver(ExecutorService service) {
		this.service = service;
	}

	@Override
	public ISolverOutput solve(ISolverInput input) {
		getParams(input);

		ISolverOutput output = new SolverOutput();
		List<Bee> scoutBees = newBeeInstances(n);
		computeMakespans(scoutBees);
		fillSolverOutput(scoutBees.get(0), 0, output);

		for (int i = 0; i < imax; ++i) {
			Collections.sort(scoutBees);
			updateSolverOutput(scoutBees.get(0), i, output);

			List<Bee> bestBees = scoutBees.subList(0, e);
			List<Bee> selectedBees = scoutBees.subList(e, m); // = selected bees without best ones

			scoutBees = new ArrayList<>(scoutBees.size());
			for (Bee bee : bestBees) {
				Bee fittestBee = searchNeighbourhood(bee, nep);
				scoutBees.add(fittestBee);
			}

			for (Bee bee : selectedBees) {
				Bee fittestBee = searchNeighbourhood(bee, nsp);
				scoutBees.add(fittestBee);
			}

			scoutBees.addAll(newBeeInstances(n - m));
		}

		Collections.sort(scoutBees);
		updateSolverOutput(scoutBees.get(0), imax, output);

		return output;
	}

	private void getParams(ISolverInput input) {
		n = Integer.parseInt(input.getParam(SCOUT_BEES_NUMBER));
		m = Integer.parseInt(input.getParam(SITES_SELECTED_NUMBER));
		e = Integer.parseInt(input.getParam(BEST_SITES_NUMBER));
		nep = Integer.parseInt(input.getParam(RECRUITED_FOR_BEST_SITES_NUMBER));
		nsp = Integer.parseInt(input.getParam(RECRUITED_FOR_OTHER_SITES_NUMBER));
		ngh = Integer.parseInt(input.getParam(PATCH_SIZE));
		imax = Integer.parseInt(input.getParam(MAX_ALGORITHM_STEPS_NUMBER));

		testInstance = input.getTestInstance();
	}

	private List<Bee> newBeeInstances(int beesNumber) {
		List<Bee> bees = new ArrayList<>(beesNumber);

		for (int i = 0; i < beesNumber; ++i) {
			TaskPermutation permutation = TaskPermutation.shuffled(testInstance.getNumOfJobs());
			Bee bee = new Bee(permutation, testInstance);

			bees.add(bee);
		}

		return bees;
	}

	private void computeMakespans(Collection<Bee> scoutBees) {
		List<Future<?>> submittedScouts = new ArrayList<>(scoutBees.size());

		for (Bee scout : scoutBees) {
			submittedScouts.add(service.submit(Executors.callable(scout)));
		}

		for (Future<?> submittedScout : submittedScouts) {
			try {
				submittedScout.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private Bee searchNeighbourhood(Bee bee, int recruitedBeesNumber) {
		List<Bee> recruitedBees = recruitBees(bee, recruitedBeesNumber);

		computeMakespans(recruitedBees);
		recruitedBees.add(bee);
		Collections.sort(recruitedBees);

		return recruitedBees.get(0);
	}

	private List<Bee> recruitBees(Bee bee, int recruitedBeesNumber) {
		List<Bee> recruitedBees = new ArrayList<>(recruitedBeesNumber);

		for (int j = 0; j < recruitedBeesNumber; ++j) {
			TaskPermutation flowerPatchNearby = TaskPermutation.shuffled(bee.getPermutation(), ngh);

			Bee recruitedBee = new Bee(flowerPatchNearby, testInstance);
			recruitedBees.add(recruitedBee);
		}

		return recruitedBees;
	}

	private void updateSolverOutput(Bee ultimateBee, int iteration, ISolverOutput output) {
		if (ultimateBee.getMakespan() < output.getMakespan()) {
			fillSolverOutput(ultimateBee, iteration, output);
		}
	}

	private void fillSolverOutput(Bee ultimateBee, int iteration, ISolverOutput output) {
		output.setIterations(iteration);
		output.setMakespan(ultimateBee.getMakespan());
		output.setOrder(ultimateBee.getPermutation().asList());
	}
}
