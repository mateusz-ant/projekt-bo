package pl.edu.agh.informatyka.bo.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class SwitchModeListener implements ActionListener{

    private boolean taillard;
    private InputPanel panel;

    public SwitchModeListener(InputPanel panel, boolean taillard) {
        this.taillard = taillard;
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.switchMode(taillard);
    }
}
