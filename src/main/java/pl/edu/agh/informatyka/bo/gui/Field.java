package pl.edu.agh.informatyka.bo.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 11/10/13
 * Time: 11:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Field extends JPanel {

    private JPanel left = new JPanel();
    private JPanel right = new JPanel();

    public Field(JComponent leftComponent, JComponent rightComponent) {

        setLayout( new GridLayout(1, 2));

        left.setLayout( new FlowLayout(FlowLayout.RIGHT) );
        right.setLayout( new FlowLayout(FlowLayout.LEFT) );

        add(left);
        add(right);

        left.add(leftComponent);
        right.add(rightComponent);
    }



}
