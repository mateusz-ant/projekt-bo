package pl.edu.agh.informatyka.bo.cockroach;

import static java.lang.Math.max;

public class Scheduler {
    Task[] tasks;
    int machines;
    int upperBound = -1;
    
    public Scheduler(Task[] tasks, int machines) {
        this.tasks = tasks;
        this.machines = machines;
    }

    public Scheduler(int[][] tasksArray, int machines, int upperBound) {
        this.tasks = new Task[tasksArray.length];
        for(int i = 0; i < tasksArray.length; i++) {
            tasks[i] = new Task(tasksArray[i]);
        }
        this.machines = machines;
        this.upperBound = upperBound;
    }
    
    public int getScheduleTime(int[] permutation) {
        int machineTime[] = new int[machines];
        
        for(int i = 0; i < machines; i++) {
            machineTime[i] = 0;
        }
        
        for(int i = 0; i < permutation.length; i++) {
            machineTime[0] = machineTime[0] + tasks[permutation[i]].getExecutionTime(0);
            
            for(int machine = 1; machine < machines; machine++) {
                machineTime[machine] = max(machineTime[machine - 1], machineTime[machine])
                        + tasks[permutation[i]].getExecutionTime(machine);
            }
        }
        
        return machineTime[machines - 1];
    }
    
    public Task[] getTasks() {
        return tasks;
    }
    
    public int getMachinesNumber() {
        return this.machines;
    }
    
    public int getTasksNumber() {
        return this.tasks.length;
    }
    
    public int getUpperBound() {
        return this.upperBound;
    }
}
