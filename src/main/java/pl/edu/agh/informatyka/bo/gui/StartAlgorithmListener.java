package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.bee.BeeSolver;
import pl.edu.agh.informatyka.bo.cockroach.CockroachAlgorithm;
import pl.edu.agh.informatyka.bo.firefly.FireflySolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.SolverInput;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 12:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class StartAlgorithmListener implements ActionListener {

    private MainWindow wnd;
    private AlgorithmPanel algorithmPanel;
    private ResultsPanel resultsPanel;
    private InputPanel inputPanel;

    public StartAlgorithmListener(MainWindow wnd, InputPanel inputPanel, AlgorithmPanel algorithmPanel, ResultsPanel resultsPanel) {
        this.wnd = wnd;
        this.inputPanel = inputPanel;
        this.algorithmPanel = algorithmPanel;
        this.resultsPanel = resultsPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ISolverInput input = new SolverInput();

        try {
            algorithmPanel.fillInput(input);
            inputPanel.fillInput(input);

            // algorithm execution dialog window
            Object[] options = { "Stop" };
            JOptionPane optionPane = new JOptionPane(
                    "Trwa obliczanie rozwiązania.\nProszę cierpliwie czekać na wynik...",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]
            );

            JDialog dialog = new JDialog(wnd, "Trwa uruchomienie", true);
            dialog.setContentPane(optionPane);
//            dialog.setDefaultCloseOperation(
//                    JDialog.DO_NOTHING_ON_CLOSE);
            dialog.pack();
            dialog.setLocationRelativeTo(wnd);

            // the algorithm execution
            AlgorithmExecutor executor = new AlgorithmExecutor( input, algorithmPanel.getSolver(), optionPane );
            Thread thread = new Thread( executor );
            optionPane.addPropertyChangeListener( new DialogListener(dialog, thread) );

            thread.start();

            // for measuring execution time
//            long startTime = System.nanoTime();
//            long executionTime;

            dialog.setVisible( true );

            // compute the execution time
//            executionTime = System.nanoTime() - startTime;

            // execution done - obtain the output
            ISolverOutput output = executor.getOutput();

            if ( output != null ){
                resultsPanel.updateOutput(output, input.getTestInstance(), executor.getExecutionTime() );

                String algorithmName = "???";
                if ( algorithmPanel.getSolver() instanceof FireflySolver ){
                    algorithmName = "SWIETLIK";
                }else if ( algorithmPanel.getSolver() instanceof CockroachAlgorithm){
                    algorithmName = "KARALUCH";
                }else if( algorithmPanel.getSolver() instanceof BeeSolver){
                    algorithmName = "PSZCZOLA";
                }

                // export results to file
                StringBuilder sb = new StringBuilder();
                sb.append( "---------------------------------------------------------\r\n" );
                sb.append(
                        String.format(
                            "Jobs:    \t%d\r\nMachines:\t%d\r\n\r\nAlgorithm:\t%s\r\nMakespan:\t%d\r\nIteration:\t%d\r\nTime:    \t%d\r\n",
                            input.getTestInstance().getNumOfJobs(),
                            input.getTestInstance().getNumOfMachines(),
                            algorithmName,
                            output.getMakespan(),
                            output.getIterations(),
                            executor.getExecutionTime() / 1000 / 1000
                        )
                );
                sb.append( "---------------------------------------------------------\r\n" );
//                FileDumper.getInstance().getStream().println( sb.toString() );
            }
        } catch (MissingParameterException e1) {
            JOptionPane.showMessageDialog( wnd, e1.getMessage() );
        } catch (NumberFormatException e1){
            JOptionPane.showMessageDialog( wnd, e1.getMessage().split("\"")[1] + " nie jest poprawną wartościa parametru." );
//            JOptionPane.showMessageDialog( wnd, e1.getMessage() );
        }
    }

}
