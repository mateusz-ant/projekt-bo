package pl.edu.agh.informatyka.bo.cockroach;

public class Task {
    int executionTimes[];
    
    public Task(int executionTimes[]) {
        this.executionTimes = executionTimes;
    }
    
    public int getExecutionTime(int machine) {
        return executionTimes[machine];
    }
}
