package pl.edu.agh.informatyka.bo.gui;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/13/13
 * Time: 11:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScaleSliderListener implements ChangeListener {

    private ResultsPanel panel;

    public ScaleSliderListener(ResultsPanel resultsPanel) {
        panel = resultsPanel;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        panel.updateScale();
    }
}
