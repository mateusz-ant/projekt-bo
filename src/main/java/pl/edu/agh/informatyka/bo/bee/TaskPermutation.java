package pl.edu.agh.informatyka.bo.bee;

import java.util.*;

class TaskPermutation {
	private List<Integer> permutation;

	private TaskPermutation() {
		permutation = new ArrayList<>();
	}

	private TaskPermutation(int tasksCount) {
		this();
		for (int i = 0; i < tasksCount; ++i)
			permutation.add(i);
	}

	private TaskPermutation(List<Integer> integers) {
		permutation = new ArrayList<>(integers);
	}

	public static TaskPermutation ordered(int tasksCount) {
		return new TaskPermutation(tasksCount);
	}

	public static TaskPermutation shuffled(int taskCount) {
		return shuffled(ordered(taskCount));
	}

	public static TaskPermutation shuffled(TaskPermutation permutation) {
		TaskPermutation shuffled = new TaskPermutation(permutation.permutation);
		Collections.shuffle(shuffled.permutation);

		return shuffled;
	}

	public static TaskPermutation shuffled(TaskPermutation permutation, int swapsNumber) {
		TaskPermutation shuffled = new TaskPermutation(permutation.permutation);
		TaskPermutation.shuffle(shuffled.permutation, swapsNumber);

		return shuffled;
	}

	private static void shuffle(List<Integer> permutation, int swapsNumber) {
		Random random = new Random(new Date().getTime());
		int size = permutation.size();

		for (int i = 0; i < swapsNumber; ++i) {
			int first = random.nextInt(size);
			int second = random.nextInt(size);

			Collections.swap(permutation, first, second);
		}
	}

	public int get(int i) {
		return permutation.get(i);
	}

	public List<Integer> asList() {
		return Collections.unmodifiableList(permutation);
	}
}
