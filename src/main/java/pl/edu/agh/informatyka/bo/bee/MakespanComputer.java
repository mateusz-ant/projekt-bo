package pl.edu.agh.informatyka.bo.bee;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

/**
 * Generic class computing total makespan of flowshop problem
 */
class MakespanComputer {

	private TestInstance testInstance;

	public MakespanComputer(TestInstance testInstance) {
		this.testInstance = testInstance;
	}

	public int compute(TaskPermutation taskPermutation) {

		final int MACHINES_NUMBER = testInstance.getNumOfMachines();
		final int TASKS_NUMBER = testInstance.getNumOfJobs();

		List<IMachine> machines = getMachines(MACHINES_NUMBER);

		for (int t = 0; t < TASKS_NUMBER; ++t) {
			int taskNum = taskPermutation.get(t);

			for (int machineNum = 0; machineNum < MACHINES_NUMBER; ++machineNum) {
				int time = testInstance.getTime(taskNum, machineNum);

				machines.get(machineNum).doTask(time);
			}
		}

		IMachine lastMachine = machines.get(machines.size() - 1);

		return lastMachine.getMakespan();
	}

	private List<IMachine> getMachines(int machinesNumber) {
		IMachine idleMachine = new IdleMachine();
		List<IMachine> machines = new ArrayList<>();
		machines.add(idleMachine);

		for (int i = 0; i < machinesNumber; ++i) {
			IMachine previous = machines.get(i);
			machines.add(new Machine(previous));
		}

		// returns all machines except idle one
		return machines.subList(1, machines.size());
	}
}

interface IMachine {
	boolean hasDoneTask(int taskNumber);

	void doTask(int taskCost);

	int getMakespanAfterTask(int taskNumber);

	int getMakespan();
}

class Machine implements IMachine {
	private IMachine previousMachine;
	private ArrayList<Integer> taskMakespan;

	public Machine(IMachine previous) {
		this.previousMachine = previous;
		this.taskMakespan = new ArrayList<>();
	}

	public boolean hasDoneTask(int taskNumber) {
		return taskNumber < taskMakespan.size();
	}

	public int getMakespanAfterTask(int taskNumber) {
		taskNumber = Math.min(taskNumber, taskMakespan.size() - 1);

		return taskMakespan.get(taskNumber);
	}

	public int getMakespan() {
		int lastTask = taskMakespan.size() - 1;

		return lastTask < 0 ? 0 : taskMakespan.get(lastTask);
	}

	public void doTask(int taskCost) {
		int newTaskNumber = taskMakespan.size();

		if (newTaskNumber > 0 && !this.hasDoneTask(newTaskNumber - 1))
			throw new IllegalStateException("Previous task isn't done on this machine");

		if (!previousMachine.hasDoneTask(newTaskNumber))
			throw new IllegalStateException("Previous part of this task isn't done on previous machine");

		int prevMachineMakespan = previousMachine.getMakespanAfterTask(newTaskNumber);
		int myMakespan = Math.max(prevMachineMakespan, getMakespan()) + taskCost;

		taskMakespan.add(myMakespan);
	}
}

/**
 * Machine handling corner case of the first "normal" machine, that doesn't have its previous machine
 */
class IdleMachine implements IMachine {

	@Override
	public boolean hasDoneTask(int taskNumber) {
		return true;
	}

	@Override
	public void doTask(int taskCost) {
	}

	@Override
	public int getMakespanAfterTask(int taskNumber) {
		return 0;
	}

	@Override
	public int getMakespan() {
		return 0;
	}
}
