package pl.edu.agh.informatyka.bo.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 11/10/13
 * Time: 10:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class CoolPanel extends JPanel {

    protected JPanel fieldsPanel = new JPanel();

    public CoolPanel() {
        super();

        setLayout( new BorderLayout() );
        setBorder( BorderFactory.createEmptyBorder(10, 10, 10, 10) );

        fieldsPanel.setLayout(new BoxLayout(fieldsPanel, BoxLayout.PAGE_AXIS));

        add(fieldsPanel, BorderLayout.CENTER);
    }

    public void addField(Field newField){
        fieldsPanel.add(newField);
    }

}
