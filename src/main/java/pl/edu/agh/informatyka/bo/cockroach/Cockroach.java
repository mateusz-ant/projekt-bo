package pl.edu.agh.informatyka.bo.cockroach;

import java.util.Random;

public class Cockroach {
    private int[] permutation;
    private int[] transitions;

    public Cockroach(int elements) {
        this.randomize(elements);
    }

    public Cockroach(int[] permutation) {
        this.setPermutation(permutation);
    }

    public Cockroach(Cockroach cockroach) {
        setPermutation(cockroach.getPermutation().clone());
    }

    public int getValue(Scheduler scheduler) {
        return scheduler.getScheduleTime(permutation);
    }

    public int[] getPermutation() {
        return permutation;
    }

    public int[] getTransitions() {
        return transitions;
    }

    public final void setPermutation(int[] permutation) {
        this.permutation = permutation;
        this.transitions = new int[permutation.length];

        for (int i = 0; i < permutation.length; i++) {
            this.transitions[permutation[i]] = i;
        }
    }

    private void randomize(int length) {
        int permut[] = new int[length];
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            permut[i] = i;
        }
        
        int randomPosition;
        int tmp;

        for (int i = 0; i < length; i++) {
            randomPosition = i + random.nextInt(length - i);
            
            tmp = permut[i];
            permut[i] = permut[randomPosition];
            permut[randomPosition] = tmp;
        }

        this.setPermutation(permut);
    }

    public void makeRandomStep() {
        Random random = new Random();
        int i = random.nextInt(permutation.length);
        int j = random.nextInt(permutation.length);

        int tmp = permutation[i];
        permutation[i] = permutation[j];
        permutation[j] = tmp;

        transitions[permutation[i]] = i;
        transitions[permutation[j]] = j;
    }

    public void makeStepTowards(Cockroach cockroach) {
        inversionSort(cockroach, true);
    }

    public int getDistance(Cockroach cockroach) {
        return inversionSort(cockroach, false);
    }

    public int inversionSort(Cockroach cockroach, boolean reallyDoInversion) {
        int hisTable[] = cockroach.getPermutation();
        int ownTable[] = permutation;
        int ownTrans[] = transitions;
        if (!reallyDoInversion) {
            ownTable = permutation.clone();
            ownTrans = transitions.clone();
        }

        int current;
        int oldEle, oldPos;
        int swaps = 0;

        for (current = 0; current < ownTable.length; current++) {
            if (ownTable[current] != hisTable[current]) {
                swaps++;

                oldEle = ownTable[current];
                oldPos = ownTrans[hisTable[current]];

                ownTable[current] = hisTable[current];
                ownTable[oldPos] = oldEle;

                ownTrans[oldEle] = oldPos;
                ownTrans[ownTable[current]] = current;

                if (reallyDoInversion) {
                    return 1;
                }
            }
        }

        return swaps;
    }
}
