package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.ISolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/18/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlgorithmExecutor implements Runnable {

    private ISolverOutput output = null;
    private ISolverInput input;
    private ISolver solver;
    private NumberFormatException exception = null;

    private JOptionPane optionPane;

    private long executionTime = 0;

    public AlgorithmExecutor(ISolverInput input, ISolver solver, JOptionPane optionPane) {
        this.input = input;
        this.solver = solver;

        this.optionPane = optionPane;
    }

    @Override
    public void run() {

        try {
            long startTime = System.nanoTime();
            output = solver.solve( input );
            executionTime = System.nanoTime() - startTime;
        } catch (NumberFormatException e) {
            exception = e;
        }

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        optionPane.firePropertyChange( "done", 0, 1 );
    }

    public ISolverOutput getOutput() throws NumberFormatException{

        if ( exception != null ){
            throw exception;
        }

        return output;
    }

    public long getExecutionTime(){
        return executionTime;
    }

}
