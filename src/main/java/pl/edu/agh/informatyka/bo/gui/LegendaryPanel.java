package pl.edu.agh.informatyka.bo.gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/17/13
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class LegendaryPanel extends JPanel {

    private List<Integer> order;

    private static final int VERTICAL_OFFSET = 10;
    private static final int VERTICAL_STEP = 30;
    private static final int SQUARE_SIZE = 10;
    private static final int HORIZONTAL_OFFSET = 5;
    private static final int HORIZONTAL_STEP = 5;

    public LegendaryPanel() {
        this.order = new ArrayList<>();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension( VERTICAL_OFFSET + ( SQUARE_SIZE + VERTICAL_STEP ) * order.size(), 30 );
    }

    @Override
    public void update(Graphics g){
        paint(g);
    }

    @Override
    public void paint(Graphics g) {

        g.clearRect( 0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE );

        for (int i = 0; i < order.size(); i++) {

            int x = VERTICAL_OFFSET + i * ( SQUARE_SIZE + VERTICAL_STEP );

            g.setColor( AwesomeOutputPanel.colors[i % AwesomeOutputPanel.colors.length] );
            g.fillRect( x, HORIZONTAL_OFFSET, SQUARE_SIZE, SQUARE_SIZE );

            g.setColor( Color.BLACK );
            g.drawString( String.valueOf( order.get(i) + 1 ), x, HORIZONTAL_OFFSET + SQUARE_SIZE + HORIZONTAL_STEP + 10 );
        }

    }

    public void setData(List<Integer> order) {
        this.order = order;
    }
}
