package pl.edu.agh.informatyka.bo.bee;

import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

class Bee implements Runnable, Comparable<Bee> {
	private static final int NOT_COMPUTED_MAKESPAN = -1;

	private TaskPermutation permutation;
	private TestInstance testInstance;
	private int makespan = NOT_COMPUTED_MAKESPAN;

	public Bee(TaskPermutation permutation, TestInstance testInstance) {
		this.permutation = permutation;
		this.testInstance = testInstance;
	}

	/**
	 * Informs how good flower patch (permutation) is
	 * 
	 * @return flower patch mark. Less == better.
	 */
	public int getMakespan() {
		if (makespan == NOT_COMPUTED_MAKESPAN)
			makespan = new MakespanComputer(testInstance).compute(permutation);

		return makespan;
	}

	/**
	 * Returns currently investigated flower patch (permutation)
	 */
	public TaskPermutation getPermutation() {
		return permutation;
	}

	@Override
	public void run() {
		getMakespan();
	}

	@Override
	public int compareTo(Bee other) {
		if (this.equals(other))
			return 0;

		int thisMakespan = this.getMakespan();
		int otherMakespan = other.getMakespan();

		if (thisMakespan == otherMakespan)
			return 0;

		return thisMakespan < otherMakespan ? -1 : 1;
	}
}
