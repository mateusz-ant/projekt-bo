package pl.edu.agh.informatyka.bo.flowshop;

import java.util.List;

public interface ISolverOutput {

	List<Integer> getOrder();

	int getMakespan();

	int getIterations();

	void setOrder(List<Integer> order);

	void setMakespan(int makespan);

	void setIterations(int iterations);
}
