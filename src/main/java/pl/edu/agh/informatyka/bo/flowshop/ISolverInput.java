package pl.edu.agh.informatyka.bo.flowshop;

import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

public interface ISolverInput {
	TestInstance getTestInstance();

	void setTestInstance(TestInstance instance);

	String getParam(String name);

	void setParam(String name, String value);
}
