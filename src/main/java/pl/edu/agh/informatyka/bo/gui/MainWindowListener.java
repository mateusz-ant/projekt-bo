package pl.edu.agh.informatyka.bo.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 11/10/13
 * Time: 9:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainWindowListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }
}
