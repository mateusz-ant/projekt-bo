package pl.edu.agh.informatyka.bo.firefly;

import pl.edu.agh.informatyka.bo.flowshop.ISolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.SolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FireflySolver implements ISolver {

	private JobShop jobShop;
	private List<Firefly> fireflies;
	private TestInstance test;

	public FireflySolver() {
	}

	private void initialize(ISolverInput input) {

		test = input.getTestInstance();

		int firefliesCount = Integer.parseInt( input.getParam("firefliesCount") );

		double absorption = Double.parseDouble( input.getParam("absorption") );
		double maxRandomStep = Double.parseDouble( input.getParam("maxRandomStep") );

		jobShop = new JobShop(test);

		fireflies = new ArrayList<>();
		for (int i = 0; i < firefliesCount; ++i)
			fireflies.add(new Firefly(jobShop, absorption, maxRandomStep));
	}

	public ISolverOutput solve(ISolverInput input) {

		initialize(input);

		int bestResult = Integer.MAX_VALUE;
		JobOrder bestOrder = null;

		int iterations = Integer.parseInt( input.getParam("iterations") );
        int bestIterationNumber = 0;

		for (int currentIteration = 0; currentIteration < iterations; ++currentIteration) {

			JobOrder order = performIteration();
			int result = jobShop.getMakespan(order);

			// System.out.println("makespan: " + result);

            if(result < bestResult) {
                bestResult = result;
                //bestOrder = order;
                bestOrder = new JobOrder(order);
                bestIterationNumber = currentIteration;
            }
		}

		ISolverOutput out = new SolverOutput();

		out.setOrder(bestOrder.toList());
		out.setMakespan(bestResult);
		out.setIterations(bestIterationNumber);

		JobOrder def = new JobOrder(test.getNumOfJobs());
		System.out.println("order: " + Arrays.toString(def.toList().toArray()));
		System.out.println("makespan: " + jobShop.getMakespan(def));

		return out;
	}

	private JobOrder performIteration() {

		// czy tu wolno posortowac ???

		for (Firefly currentFirefly : fireflies) {

			boolean currentMoved = false;

			for (Firefly otherFirefly : fireflies) {

				if (currentFirefly == otherFirefly)
					continue;

				if (currentFirefly.isLessAttractiveThan(otherFirefly)) {
					currentMoved = true;
					currentFirefly.moveToOther(otherFirefly);
				} else {
					// System.out.println("kurwa mac");
				}

			}

			if (!currentMoved) {
				if(!currentFirefly.performLocalSearch())
					currentFirefly.moveRandomly();
			}
		}

		int bestTime = Integer.MAX_VALUE;
		JobOrder bestOrder = null;

		for (Firefly f : fireflies) {
			int currentTime = jobShop.getMakespan(f.getJobOrder());
			if (currentTime < bestTime) {
				bestTime = currentTime;
				bestOrder = f.getJobOrder();
			}
		}

		return bestOrder;
	}
}
