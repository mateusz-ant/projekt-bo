package pl.edu.agh.informatyka.bo.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 11/10/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainWindow extends JFrame {

    public MainWindow() throws HeadlessException {
        super("Cool Flowshop");

        initWindow();

//        initMenuBar();

        JTabbedPane pane = initJTabbedPane();

        initTabs(pane);

        this.pack();
        setSize(800, 600);
    }

    private void initTabs(JTabbedPane pane) {

        InputPanel inputPanel = new InputPanel();
        inputPanel.bindBrowse(this);
        ResultsPanel fireflyResultsPanel = new ResultsPanel();
        ResultsPanel beeResultsPanel = new ResultsPanel();
        ResultsPanel cockroachResultsPanel = new ResultsPanel();
        AlgorithmPanel fireflyPanel = AlgorithmPanelFactory.createFireflyPanel(this, inputPanel, fireflyResultsPanel);
        AlgorithmPanel beePanel = AlgorithmPanelFactory.createBeePanel(this, inputPanel, beeResultsPanel);
        AlgorithmPanel cockroachPanel = AlgorithmPanelFactory.createCockroachPanel(this, inputPanel, cockroachResultsPanel);

        pane.addTab( "Dane", inputPanel );

        pane.addTab( "Świetlik", fireflyPanel );
        pane.addTab( "Wyniki świetlika", fireflyResultsPanel );

        pane.addTab( "Pszczoła", beePanel );
        pane.addTab( "Wyniki pszczoły", beeResultsPanel );

		pane.addTab( "Karaluch", cockroachPanel );
        pane.addTab( "Wyniki karalucha", cockroachResultsPanel );
    }

    private void initWindow() {
        setVisible(true);
        addWindowListener( new MainWindowListener() );
        setLayout(new BorderLayout());
    }

    private JTabbedPane initJTabbedPane() {
        JTabbedPane pane = new JTabbedPane();
        this.add(pane, BorderLayout.CENTER);
        return pane;
    }

    private void initMenuBar() {
        MenuBar menuBar = new MenuBar();
        setMenuBar(menuBar);
        Menu menuFile = new Menu("File");
        menuFile.add("Exit");
//        menuFile.addActionListener(this);
        menuBar.add(menuFile);
    }

}
