package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.util.TaillardLoader;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrowseFileListener implements ActionListener {

    private MainWindow wnd;
    private JTextField pathField;
    private JComboBox combobox;

    public BrowseFileListener(MainWindow wnd, JTextField pathField, JComboBox<String> combobox) {
        this.wnd = wnd;
        this.pathField = pathField;
        this.combobox = combobox;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // let the user pick the file
        JFileChooser fc = new JFileChooser( Paths.get("").toAbsolutePath().toString() );

        int resultVal = fc.showOpenDialog(wnd);

        if (resultVal == JFileChooser.APPROVE_OPTION){
            // if successfully choosen, memorize in text field
            String filename = fc.getSelectedFile().getAbsolutePath();
            pathField.setText( filename );

            try {
                TaillardLoader loader = new TaillardLoader();
                loader.loadFile(filename);

                List<TestInstance> instances = loader.getInstances();

                // update the combobox
                combobox.removeAllItems();
                for (int i = 0; i < instances.size(); i++) {
                    combobox.addItem( new ComboItem( String.valueOf(i + 1), instances.get(i) ) );
                }

            } catch(InputMismatchException e1) {
                JOptionPane.showMessageDialog(wnd, "Podany plik nie jest poprawnym plikiem w formacie Taillarda.");
            }
        }

    }
}

