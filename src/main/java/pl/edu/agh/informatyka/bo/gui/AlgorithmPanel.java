package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.ISolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 11/11/13
 * Time: 1:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class AlgorithmPanel extends CoolPanel {

    private Map<String, JTextField> fieldMap = new HashMap<String, JTextField>();
    private Map<String, String> labelMap = new HashMap<String, String>();
    private ISolver solver;
    private JButton startButton;

    public AlgorithmPanel() {
        super();

        // panel for START button
        startButton = new JButton("START");
        JPanel execPanel = new JPanel();
        execPanel.setLayout( new GridLayout(1, 1) );
        execPanel.add( startButton );
        add(execPanel, BorderLayout.AFTER_LINE_ENDS);
    }

    public void addParam(String name, String label){
        JTextField textField = new JTextField(4);
        fieldMap.put( name, textField );
        labelMap.put( name, label );

        addField( new Field( new JLabel(label + ":"), textField) );
    }

    public String getParam(String name){
        return fieldMap.get(name).getText();
    }

    public void setSolver(ISolver solver){
        this.solver = solver;
    }

    public ISolver getSolver(){
        return solver;
    }

    public void fillInput(ISolverInput input) throws MissingParameterException{
        for ( Map.Entry<String, JTextField> entry : fieldMap.entrySet() ){

            if ( "".equals( entry.getValue().getText() ) ){
                throw new MissingParameterException( "Nie określono parametru " + labelMap.get( entry.getKey() ) + "." );
            }

            input.setParam( entry.getKey(), entry.getValue().getText() );
        }
    }

    public void load(String filename){
        //TODO
    }

    public void save(String filename){
        //TODO
    }

    public void bindExecution(MainWindow wnd, InputPanel inputPanel, ResultsPanel resultsPanel) {
        startButton.addActionListener( new StartAlgorithmListener(wnd, inputPanel, this, resultsPanel) );
    }

}
