package pl.edu.agh.informatyka.bo.cockroach;

import pl.edu.agh.informatyka.bo.flowshop.ISolver;
import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.SolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CockroachAlgorithm implements ISolver {
    Cockroach cockroaches[];
    Scheduler scheduler;
    Random random;
    private TestInstance test;
    
    // Maximum visibility (number of permutation swaps) of cockroaches
    int visibility;
    // Number of random steps for cockroach to take in each iteration
    int disperse;
    // Probablility of duplicating best cockroach in each iteration
    double duplicateProbability;
    
    // Information about globally best solution
    Cockroach bestCockroach;
    int bestSolution = 0;
    int bestIteration = 0;
    
    // Current iteration
    int iteration = 0;
    
    public CockroachAlgorithm() {
        
    }
    
    private void initialize(ISolverInput input) {
        test = input.getTestInstance();

        int cockroachesCount = Integer.parseInt( input.getParam("cockroachesCount") );
        this.visibility = Integer.parseInt(input.getParam("visibility"));
        this.disperse = Integer.parseInt(input.getParam("randomSteps"));
        this.duplicateProbability = Double.parseDouble(input.getParam("duplicateProbability"));

        int[][] machineTimes = test.getTimes();
        int machines = test.getNumOfMachines();
        int upperBound = test.getUpperBound();

        this.random = new Random();

        input.getTestInstance();
        this.scheduler = new Scheduler(machineTimes, machines, upperBound);

        this.cockroaches = new Cockroach[cockroachesCount];
        
        for(int i = 0; i < cockroachesCount; i++) {
            this.cockroaches[i] = new Cockroach(scheduler.getTasksNumber());
        }
    }
    
    
    @Override
    public ISolverOutput solve(ISolverInput input) {
    	 initialize(input);
    	
    	 this.bestCockroach = new Cockroach(this.cockroaches[0]);
         this.bestSolution = this.bestCockroach.getValue(scheduler);
         
         this.findBestSolution();
         
         int iterations = Integer.parseInt(input.getParam("iterations"));

         // inserted by Pawel Mikolajczyk
         this.iteration = 0;

         for (int i = 0; i < iterations; i++) {
        	 runIteration();
         }
         
         ISolverOutput out = new SolverOutput();
         
         Integer[] bestOrder = new Integer[scheduler.getTasksNumber()];
         for (int i = 0; i < scheduler.getTasksNumber(); i++) {
        	 bestOrder[i] = new Integer(bestCockroach.getPermutation()[i]);
         }
         
         List<Integer> order = Arrays.asList(bestOrder);
         
         out.setOrder(order);
         out.setMakespan(bestSolution);
         out.setIterations(bestIteration);
         
         return out;

    }
    
    private void findBestSolution() {
        for (Cockroach cockroach : cockroaches) {
            int solution = cockroach.getValue(scheduler);
            if (solution < this.bestSolution) {
                this.bestCockroach = new Cockroach(cockroach);
                this.bestSolution = solution;
                this.bestIteration = iteration;
            }
        }
    }
    
    private void runIteration() {
        iteration++;
        
        this.moveCockroaches();
        this.disperseCockroaches();
        this.duplicateBestCockroach();

    }

    private void moveCockroaches() {
        Cockroach[] movedCockroaches = new Cockroach[cockroaches.length];
        
        Cockroach current;
        Cockroach bestVisibleCockroach;
        int bestVisibleSolution;
        int bestVisibleDistance;
        
        for(int i = 0; i < cockroaches.length; i++) {
            current = new Cockroach(cockroaches[i]);
            
            bestVisibleCockroach = null;
            bestVisibleSolution = 0;
            bestVisibleDistance = 0;
            
            for(int j = 0; j < cockroaches.length; j++) {
                if(i == j) {
                    continue;
                }
                
                int distance = cockroaches[j].getDistance(current);
                if(distance > visibility) {
                    continue;
                }
                        
                if(bestVisibleCockroach == null || bestVisibleSolution > cockroaches[j].getValue(scheduler)) {
                    bestVisibleCockroach = cockroaches[j];
                    bestVisibleSolution = cockroaches[j].getValue(scheduler);
                    bestVisibleDistance = distance;
                }
            }
            
            if(bestVisibleCockroach == null) {
                bestVisibleCockroach = bestCockroach;
                bestVisibleSolution = bestSolution;
                bestVisibleDistance = bestCockroach.getDistance(current);
            }
            
            if(bestVisibleDistance != 0) {
                int minimum = current.getValue(scheduler);
                int value[] = current.getPermutation().clone();

                for (int step = 0; step < bestVisibleDistance; step++) {
                    current.makeStepTowards(bestVisibleCockroach);

                    if (current.getValue(scheduler) < minimum) {
                        minimum = current.getValue(scheduler);
                        value = current.getPermutation().clone();
                    }
                }

                current.setPermutation(value);
            }
            
            movedCockroaches[i] = current;
        }
        
        this.cockroaches = movedCockroaches;
        this.findBestSolution();
    }

    private void disperseCockroaches() {
        for(int steps = 0; steps < disperse; steps++) {
            for (Cockroach cockroach : cockroaches) {
                cockroach.makeRandomStep();
            }
            
            this.findBestSolution();
        }
    }

    private void duplicateBestCockroach() {
        if(duplicateProbability > Math.random()) {
            int j = random.nextInt(cockroaches.length);
            cockroaches[j] = new Cockroach(bestCockroach);
        }
    }
    
    public void printInfo() {
        System.out.println("Best result "+bestSolution+" in #"+bestIteration);
        System.out.println("Solution: "+Arrays.toString(bestCockroach.getPermutation()));
    }
    
    public int getBestSolution() {
        return this.bestSolution;
    }
}
