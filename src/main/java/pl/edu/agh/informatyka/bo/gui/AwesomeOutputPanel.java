package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/8/13
 * Time: 12:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class AwesomeOutputPanel extends JPanel {

    public static final Color colors[] = {
            new Color(1.0f, 0.0f, 0.0f),
            new Color(0.0f, 1.0f, 0.0f),
            new Color(0.0f, 0.0f, 1.0f),
            new Color(1.0f, 1.0f, 0.0f),
            new Color(0.0f, 1.0f, 1.0f),
            new Color(1.0f, 0.0f, 1.0f),
            new Color(1.0f, 0.5f, 0.0f),
            new Color(0.5f, 0.0f, 0.8f),
            new Color(0.0f, 0.5f, 0.25f),
            new Color(0.5f, 0.25f, 0.0f),
    };
    private static final int INFINITY = 99999;
    private static final int BAR_HEIGHT = 10;
    private static final int VERTICAL_STEP = 10;
    private static final int PIXELS_PER_TICK = 1;
    private static final int TIME_AXIS_STEP = 100;

    private ISolverOutput output;
    private TestInstance testInstance;

    private List< List<Rectangle> > rectangles = new ArrayList<>();
    private Point origin;
    private int scaleX = 100;
    private int scaleY = 100;

    public AwesomeOutputPanel(ISolverOutput output, TestInstance testInstance, int scaleX, int scaleY) {
        origin = new Point(40, 40);

        this.scaleX = scaleX;
        this.scaleY = scaleY;

        this.output = output;
        this.testInstance = testInstance;

        setData();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension( (int) ( origin.x + output.getMakespan() * getPixelsPerTick() + 100 ), (int) ( origin.y + testInstance.getNumOfMachines() * ( getBarHeight() + getVerticalStep() ) + 50 ) );
    }

    @Override
    public void paint(Graphics g) {

        g.clearRect(0, 0, INFINITY, INFINITY);

        g.drawLine(origin.x, origin.y, INFINITY, origin.y);
        g.drawLine(origin.x, origin.y, origin.x, INFINITY);

        // machine numbers on Y axis
        for (int i = 0; i < testInstance.getNumOfMachines(); i++) {
            g.drawString( String.format( "%3d", i + 1 ), origin.x - 20, (int) (origin.y + (i + 1) * (getVerticalStep() + getBarHeight())));
        }

        // timestamps on X axis
        for (int i = 0; i < INFINITY; i += getTimeAxisStep()) {
            int x = (int) (origin.x + getPixelsPerTick() * i);
            g.drawString( String.format( "%d", i ), x, origin.y - 10 );
            g.drawLine( x, origin.y, x, INFINITY );
        }

        int colorID = 0;

        for ( List<Rectangle> jobList : rectangles ){

            g.setColor( colors[ colorID ] );

            for ( Rectangle rect : jobList ){
                g.fillRect( rect.x, rect.y, rect.width, rect.height );
            }

            colorID = (colorID + 1) % colors.length;

        }
    }

    @Override
    public void update(Graphics g){
        paint(g);
    }

    private void setData(){
        int times[][] = testInstance.getTimes();
        rectangles = new ArrayList<>();
        int lastTick = 0;
        int lineEnds[] = new int[ testInstance.getNumOfMachines() ];    // per row
        int x;
        int y;
        int width;

        for ( int jobID : output.getOrder() ){

            rectangles.add( new ArrayList<Rectangle>() );
            lastTick = 0;

            for (int row = 0; row < testInstance.getNumOfMachines(); row++) {
                x = Math.max( lastTick, lineEnds[row] );
                y = (int) ( getVerticalStep() + ( getVerticalStep() + getBarHeight() ) * row );
                width = (int) ( testInstance.getTime( jobID, row ) * getPixelsPerTick() );
                lastTick = x + width;
                lineEnds[row] = lastTick;

//                System.out.println( x + width );

                rectangles.get( rectangles.size() - 1 ).add(
                        new Rectangle(
                                origin.x + x,
                                origin.y + y,
                                width,
                                (int) getBarHeight()
                        )
                );
            }

        }

    }
    
    private double getBarHeight(){
        return BAR_HEIGHT * scaleY / 100.0;
    }

    private double getVerticalStep(){
        return VERTICAL_STEP * scaleY / 100.0;
    }

    private double getPixelsPerTick(){
        return PIXELS_PER_TICK * scaleX / 100.0;
    }

    private int getTimeAxisStep(){
        if ( scaleX <= 100 ){
            return TIME_AXIS_STEP;
        }else if ( scaleX <= 300 ){
            return TIME_AXIS_STEP / 2;
        }

        return TIME_AXIS_STEP / 10;
    }

}
