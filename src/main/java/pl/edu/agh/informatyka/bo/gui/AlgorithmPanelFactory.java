package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.bee.BeeAlgorithmParams;
import pl.edu.agh.informatyka.bo.bee.BeeSolver;
import pl.edu.agh.informatyka.bo.cockroach.CockroachAlgorithm;
import pl.edu.agh.informatyka.bo.firefly.FireflySolver;

import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/4/13
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AlgorithmPanelFactory {

    public static AlgorithmPanel createFireflyPanel(MainWindow wnd, InputPanel inputPanel, ResultsPanel resultsPanel){
        AlgorithmPanel fireflyPanel = new AlgorithmPanel();

        fireflyPanel.addParam("firefliesCount", "Ilość świetlików");
        fireflyPanel.addParam("absorption", "Absorpcja");
        fireflyPanel.addParam("maxRandomStep", "Maksymalny krok losowy");
        fireflyPanel.addParam("iterations", "Ilość iteracji");

        fireflyPanel.setSolver( new FireflySolver() );
        fireflyPanel.bindExecution(wnd, inputPanel, resultsPanel);

        return fireflyPanel;
    }

    public static AlgorithmPanel createBeePanel(MainWindow wnd, InputPanel inputPanel, ResultsPanel resultsPanel){
        AlgorithmPanel beePanel = new AlgorithmPanel();

        beePanel.addParam( BeeAlgorithmParams.SCOUT_BEES_NUMBER, "Ilość pszczół" );
        beePanel.addParam( BeeAlgorithmParams.SITES_SELECTED_NUMBER, BeeAlgorithmParams.SITES_SELECTED_NUMBER );
        beePanel.addParam( BeeAlgorithmParams.BEST_SITES_NUMBER, BeeAlgorithmParams.BEST_SITES_NUMBER );
        beePanel.addParam( BeeAlgorithmParams.RECRUITED_FOR_BEST_SITES_NUMBER, BeeAlgorithmParams.RECRUITED_FOR_BEST_SITES_NUMBER );
        beePanel.addParam( BeeAlgorithmParams.RECRUITED_FOR_OTHER_SITES_NUMBER, BeeAlgorithmParams.RECRUITED_FOR_OTHER_SITES_NUMBER );
        beePanel.addParam( BeeAlgorithmParams.PATCH_SIZE, BeeAlgorithmParams.PATCH_SIZE );
        beePanel.addParam( BeeAlgorithmParams.MAX_ALGORITHM_STEPS_NUMBER, "Ilość iteracji" );

        beePanel.setSolver( new BeeSolver( Executors.newSingleThreadExecutor() ) );
        beePanel.bindExecution( wnd, inputPanel, resultsPanel );

        return beePanel;
    }

    public static AlgorithmPanel createCockroachPanel(MainWindow wnd, InputPanel inputPanel, ResultsPanel resultsPanel){
        AlgorithmPanel cockroachPanel = new AlgorithmPanel();

        cockroachPanel.addParam("cockroachesCount", "Ilość karaluchów");
        cockroachPanel.addParam("visibility", "Widzialność karaluchów");
        cockroachPanel.addParam("randomSteps", "Ilość losowych kroków");
        cockroachPanel.addParam("duplicateProbability", "P duplikacji karalucha [0-1]");
        cockroachPanel.addParam("iterations", "Ilość iteracji");

        cockroachPanel.setSolver( new CockroachAlgorithm() );
        cockroachPanel.bindExecution( wnd, inputPanel, resultsPanel );

        return cockroachPanel;
    }
}
