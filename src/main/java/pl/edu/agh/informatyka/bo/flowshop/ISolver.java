package pl.edu.agh.informatyka.bo.flowshop;

public interface ISolver {
	ISolverOutput solve(ISolverInput input);
}
