package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.ISolverInput;
import pl.edu.agh.informatyka.bo.flowshop.util.Random;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 12:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class InputPanel extends CoolPanel {

    private Map<String, JTextField> fieldMap = new HashMap<String, JTextField>();

    private JPanel taillardPanel = new JPanel();
    private JTextField filenameField = new JTextField(40);
    private JButton browseButton = new JButton("...");
    private JComboBox<String> taillardComboBox = new JComboBox<String>();

    private JButton randomButton = new JButton("        ");
    private JButton taillardButton = new JButton("        ");

    private boolean taillardMode;

    private static int topBorder = 10;
    private static int leftBorder = 10;
    private static int bottomBorder = 10;
    private static int rightBorder = 10;

    private static String[] fieldNames = { "Ilość zadań", "Ilość maszyn", "Losowa długość zadania - min.", "Losowa długość zadania - maks.", "Ziarno", "Dolna granica", "Górna granica" };

    public InputPanel() {
        super();

        initFieldPanel();

        initTaillardSetup();

        switchMode(false);
    }

    public void addParam(String name){
        JTextField textField = new JTextField(4);
        fieldMap.put( name, textField );

        addField( new Field( new JLabel(name), textField) );
    }

    private void initTaillardSetup() {

        JPanel overTaillardPanel = new JPanel();
        overTaillardPanel.setLayout( new BorderLayout() );

        GridBagConstraints c = new GridBagConstraints();

        JPanel subTaillardPanel = new JPanel( new GridBagLayout() );
        subTaillardPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 50, 10));

        JLabel label = new JLabel("Wczytana z pliku testowa instancja Taillarda:");
        label.setBorder( BorderFactory.createEmptyBorder( 10, 0, 30, 100 ) );
        c.gridx = 0;
        c.gridy = 0;
        subTaillardPanel.add( label, c );

        c.gridx = 0;
        c.gridy = 1;
        subTaillardPanel.add(filenameField, c);

        c.gridx = 1;
        subTaillardPanel.add(browseButton, c);

        c.gridx = 2;
        subTaillardPanel.add( Box.createHorizontalStrut(50), c );

        c.gridx = 3;
        subTaillardPanel.add(taillardComboBox, c);

        taillardPanel.add( subTaillardPanel );

        taillardButton.addActionListener( new SwitchModeListener(this, true) );
        overTaillardPanel.add( taillardButton, BorderLayout.BEFORE_LINE_BEGINS );
        overTaillardPanel.add( taillardPanel, BorderLayout.CENTER );

        add(overTaillardPanel, BorderLayout.AFTER_LAST_LINE);
    }

    private void initFieldPanel() {
        JLabel label = new JLabel( "Generowane losowo dane wejściowe:" );
        label.setBorder( BorderFactory.createEmptyBorder(10, 0, 20, 0) );
        addField(new Field(label, new JLabel()));

        for (String fieldName : fieldNames){
            addParam(fieldName);
        }

        randomButton.addActionListener( new SwitchModeListener(this, false) );
        add(randomButton, BorderLayout.BEFORE_LINE_BEGINS);
    }

    private void fillRandomInput(ISolverInput input) throws MissingParameterException{

        // check if any parameter is not entered
        for ( Map.Entry<String, JTextField> entry : fieldMap.entrySet() ){
            if ( "".equals( entry.getValue().getText() ) ){
                throw new MissingParameterException( "Nie określono parametru " + entry.getKey() + "." );
            }
        }

        // obtain parameters as designed types
        int jobsCount = Integer.parseInt( fieldMap.get(fieldNames[0]).getText() );
        int machinesCount = Integer.parseInt( fieldMap.get(fieldNames[1]).getText() );
        int minDuration = Integer.parseInt( fieldMap.get(fieldNames[2]).getText() );
        int maxDuration = Integer.parseInt( fieldMap.get(fieldNames[3]).getText() );
        int initialSeed = Integer.parseInt( fieldMap.get(fieldNames[4]).getText() );
        int lowerBound = Integer.parseInt( fieldMap.get(fieldNames[5]).getText() );
        int upperBound = Integer.parseInt( fieldMap.get(fieldNames[6]).getText() );

        // fill the job durations array with random values
        int[][] durations = new int[ jobsCount ][ machinesCount ];
        for (int i = 0; i < jobsCount; i++) {
            for (int j = 0; j < machinesCount; j++) {
                durations[i][j] = Random.fromRange( minDuration, maxDuration );
            }
        }

        // finally initialize the input with designed test instance
        input.setTestInstance( new TestInstance( durations, initialSeed, lowerBound, upperBound ) );
    }

    private void fillTaillardInput(ISolverInput input){

        ComboItem selection = (ComboItem)taillardComboBox.getSelectedItem();
        input.setTestInstance( selection.getInstance() );
    }

    public void fillInput(ISolverInput input) throws MissingParameterException {

        if (taillardMode){
            fillTaillardInput(input);
        }else{
            fillRandomInput(input);
        }

    }

    public void bindBrowse(MainWindow wnd){
        browseButton.addActionListener( new BrowseFileListener(wnd, filenameField, taillardComboBox) );
    }

    public void switchMode(boolean taillard){
        taillardMode = taillard;

        if (taillard){
            taillardPanel.setBorder(BorderFactory.createMatteBorder(topBorder, leftBorder, bottomBorder, rightBorder, Color.GREEN));
            fieldsPanel.setBorder( BorderFactory.createEmptyBorder(topBorder, leftBorder, bottomBorder, rightBorder) );
        }else{
            fieldsPanel.setBorder( BorderFactory.createMatteBorder(topBorder, leftBorder, bottomBorder, rightBorder, Color.GREEN) );
            taillardPanel.setBorder(BorderFactory.createEmptyBorder(topBorder, leftBorder, bottomBorder, rightBorder));
        }
    }

}
