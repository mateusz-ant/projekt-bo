package pl.edu.agh.informatyka.bo.gui;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/18/13
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class DialogListener implements PropertyChangeListener {

    private JDialog dialog;
    private Thread thread;

    public DialogListener(JDialog dialog, Thread thread) {
        this.dialog = dialog;
        this.thread = thread;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String prop = evt.getPropertyName();

        if ( dialog.isVisible() ) {
//            System.out.println("closedddd");
            if ( !"done".equals( prop ) ){
                thread.suspend();
            }
            dialog.setVisible(false);
        }
    }


}
