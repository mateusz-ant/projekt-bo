package pl.edu.agh.informatyka.bo.flowshop.util;

import java.util.*;
import java.io.*;

/**
 * Klasa wczytujaca generyczne instancje testowe
 */
public class TaillardLoader {

	private List<TestInstance> instances;

	public List<TestInstance> getInstances() {
		return Collections.unmodifiableList(instances);
	}

	/**
	 * Laduje plik z instancjami testowymi, sformatowanymi jak tutaj: http://mistic.heig-vd.ch/taillard/problemes.dir/ordonnancement.dir/ordonnancement.html
	 * 
	 * @param fileName
	 *            instancja testowa
	 */
	public void loadFile(String fileName) {

		Scanner file = null;
		try {
			instances = new ArrayList<>();
			file = new Scanner(new File(fileName));

			while (file.hasNextLine())
				instances.add(loadInstance(file));
		} catch (FileNotFoundException e) {
		} finally {
			file.close();
		}
	}

	private TestInstance loadInstance(Scanner file) {

		file.nextLine(); // eat header
		int numOfJobs = file.nextInt();
		int numOfMachines = file.nextInt();
		int initialSeed = file.nextInt();
		int upperBound = file.nextInt();
		int lowerBound = file.nextInt();

		file.nextLine();
		file.nextLine();

		int[][] processingTimes = new int[numOfJobs][numOfMachines];

		for (int j = 0; j < numOfMachines; ++j)
			for (int i = 0; i < numOfJobs; ++i)
				processingTimes[i][j] = file.nextInt();

		file.nextLine();

		return new TestInstance(processingTimes, lowerBound, upperBound, initialSeed);
	}
}
