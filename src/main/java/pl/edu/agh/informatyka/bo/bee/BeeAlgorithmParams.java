package pl.edu.agh.informatyka.bo.bee;

public class BeeAlgorithmParams {

	/**
	 * Number of scout bees n
	 */
	public static final String SCOUT_BEES_NUMBER = "n";

	/**
	 * Number of sites selected m out of n visited sites
	 */
	public static final String SITES_SELECTED_NUMBER = "m";

	/**
	 * Number of best sites e out of m selected sites
	 */
	public static final String BEST_SITES_NUMBER = "e";

	/**
	 * Number of bees recruited for best e sites nep or n2
	 */
	public static final String RECRUITED_FOR_BEST_SITES_NUMBER = "nep";

	/**
	 * Number of bees recruited for the other (m-e) selected sites which is nsp or n1
	 */
	public static final String RECRUITED_FOR_OTHER_SITES_NUMBER = "nsp";

	/**
	 * Initial size of patches ngh which includes site and its neighbourhood and stopping criterion
	 */
	public static final String PATCH_SIZE = "ngh";

	/**
	 * Number of algorithm steps repetitions imax
	 */
	public static final String MAX_ALGORITHM_STEPS_NUMBER = "imax";
}
