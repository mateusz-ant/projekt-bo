package pl.edu.agh.informatyka.bo.gui;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class MissingParameterException extends Exception {

    public MissingParameterException(String name) {
        super(name);
    }

}
