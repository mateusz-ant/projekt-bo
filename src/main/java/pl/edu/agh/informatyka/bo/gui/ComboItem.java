package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComboItem {

    private String label;
    private TestInstance instance;

    public ComboItem(String label, TestInstance instance) {
        this.label = label;
        this.instance = instance;
    }

    @Override
    public String toString() {
        return label;
    }

    public TestInstance getInstance() {
        return instance;
    }

}
