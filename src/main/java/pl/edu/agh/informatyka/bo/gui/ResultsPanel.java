package pl.edu.agh.informatyka.bo.gui;

import pl.edu.agh.informatyka.bo.flowshop.ISolverOutput;
import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/5/13
 * Time: 12:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class ResultsPanel extends JPanel {

    private AwesomeOutputPanel image;
    private JScrollPane pan;
    private JScrollPane legendaryPan;
    private JSlider horizontalSlider;
    private JSlider verticalSlider;
    private ISolverOutput output;
    private TestInstance testInstance;
    private JLabel makespanLabel;
    private JLabel iterationsLabel;
    private JLabel exetimeLabel;
    private long executionTime;
    private LegendaryPanel legendPanel;
    private JPanel binPanel;
    private boolean filled = false;
    private JLabel legendLabel;

    private static final int LEGEND_HEIGHT = 60;

    public ResultsPanel() {
        super( new BorderLayout() );

        binPanel = new JPanel();
        binPanel.setLayout( new BoxLayout( binPanel, BoxLayout.Y_AXIS ) );
        add(binPanel);

        pan = new JScrollPane();
        binPanel.add( pan );

        JPanel bottomPanel = new JPanel( new BorderLayout() );
        bottomPanel.setBorder( BorderFactory.createEmptyBorder(50, 50, 50, 50) );

        horizontalSlider = new JSlider(0, 1, 1000, 100);
        verticalSlider = new JSlider(1, 1, 100, 100);
//        horizontalSlider.setPreferredSize( new Dimension(400, 20) );
        verticalSlider.setPreferredSize( new Dimension(20, 100) );

        horizontalSlider.addChangeListener( new ScaleSliderListener(this) );
        verticalSlider.addChangeListener( new ScaleSliderListener(this) );

        bottomPanel.add( horizontalSlider, BorderLayout.BEFORE_FIRST_LINE );
        bottomPanel.add( verticalSlider, BorderLayout.AFTER_LINE_ENDS );

        // other cool results
        JPanel bottomSubPanel = new JPanel( new GridLayout(3, 2) );
        bottomSubPanel.add( new JLabel("Sumaryczny czas wykonania prac:"), BorderLayout.CENTER );
        makespanLabel = new JLabel();
        bottomSubPanel.add( makespanLabel, BorderLayout.CENTER );
        iterationsLabel = new JLabel();
        bottomSubPanel.add( new JLabel("Iteracja, w której uzyskano optimum:"), BorderLayout.CENTER );
        bottomSubPanel.add( iterationsLabel, BorderLayout.CENTER );
        exetimeLabel = new JLabel();
        bottomSubPanel.add( new JLabel("Czas obliczeń [ms]:"), BorderLayout.CENTER );
        bottomSubPanel.add( exetimeLabel, BorderLayout.CENTER );

        // chart legend
        legendLabel = new JLabel("Optymalna kolejność wykonywania zadań:");
        legendLabel.setBorder( BorderFactory.createEmptyBorder(10, 0, 0, 0) );
        binPanel.add( legendLabel );
        legendPanel = new LegendaryPanel();
        legendaryPan = new JScrollPane( legendPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        legendaryPan.setPreferredSize( new Dimension(Integer.MAX_VALUE, LEGEND_HEIGHT ) );
        legendaryPan.setMaximumSize(new Dimension(Integer.MAX_VALUE, LEGEND_HEIGHT));
        binPanel.add( legendaryPan );

        bottomPanel.add(bottomSubPanel, BorderLayout.CENTER);

        add(bottomPanel, BorderLayout.AFTER_LAST_LINE);
    }

    public void updateOutput(ISolverOutput output, TestInstance testInstance, long executionTime) {
        this.output = output;
        this.testInstance = testInstance;
        this.executionTime = executionTime;

        makespanLabel.setText( String.valueOf( output.getMakespan() ) );
        iterationsLabel.setText( String.valueOf( output.getIterations() ) );
        exetimeLabel.setText( String.valueOf( executionTime / 1000 / 1000 ) );

        updateChart(output, testInstance);

        updateLegend();

        filled = true;
    }

    private void updateChart(ISolverOutput output, TestInstance testInstance) {
        image = new AwesomeOutputPanel( output, testInstance, horizontalSlider.getValue(), verticalSlider.getValue() );
        binPanel.remove(pan);
        pan = new JScrollPane( image );
        binPanel.add(pan);

        updateUI();
    }

    private void updateLegend(){
        legendPanel.setData(output.getOrder());

        binPanel.remove( legendLabel );
        binPanel.add( legendLabel );

        binPanel.remove( legendaryPan );
        legendaryPan = new JScrollPane( legendPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
        legendaryPan.setPreferredSize(new Dimension(Integer.MAX_VALUE, LEGEND_HEIGHT));
        legendaryPan.setMaximumSize(new Dimension(Integer.MAX_VALUE, LEGEND_HEIGHT));
        binPanel.add( legendaryPan );

        updateUI();
    }

    public void updateScale(){

        if ( filled ) {
            updateChart(output, testInstance);
            updateLegend();
        }

    }

}
