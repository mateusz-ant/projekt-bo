package pl.edu.agh.informatyka.bo.gui;

import java.io.PrintStream;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: pawel
 * Date: 12/18/13
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileDumper {

    private static FileDumper instance = new FileDumper(
            String.format(
                    "dump_%d-%d-%d___%d-%d-%d.txt",
                    Calendar.getInstance().get(Calendar.YEAR),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                    Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                    Calendar.getInstance().get(Calendar.MINUTE),
                    Calendar.getInstance().get(Calendar.SECOND)
            )
    );

    private PrintStream stream;

    private FileDumper(String filename) {
//        try {
//            File file = new File( filename );
//            stream = new PrintStream( new FileOutputStream( file, false ) );
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }

    public static FileDumper getInstance(){
        return instance;
    }

    public PrintStream getStream(){
        return stream;
    }

}
