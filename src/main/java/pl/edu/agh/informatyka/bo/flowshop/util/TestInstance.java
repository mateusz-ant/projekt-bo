package pl.edu.agh.informatyka.bo.flowshop.util;

/**
 * Instancja testowa Taillarda
 */
public class TestInstance {

	private int numOfJobs;
	private int numOfMachines;

	private int initialSeed;
	private int lowerBound;
	private int upperBound;

	// [jobNum][machineNum]
	private int[][] processingTimes;

	public TestInstance(int[][] processingTimes, int initialSeed, int lowerBound, int upperBound) {
		this.numOfJobs = processingTimes.length;
		this.numOfMachines = processingTimes[0].length;
		this.initialSeed = initialSeed;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.processingTimes = processingTimes;
	}

	/**
	 * Zwraca czas przetwarzania zadania na maszynie
	 * 
	 * @param jobNum
	 *            numer zadania
	 * @param machineNum
	 *            numer maszyny
	 */
	public int getTime(int jobNum, int machineNum) {
		return processingTimes[jobNum][machineNum];
	}

	/**
	 * Zwraca pelna tablice czasow
	 */
	public int[][] getTimes() {
		return processingTimes;
	}

	public int getNumOfJobs() {
		return numOfJobs;
	}

	public int getNumOfMachines() {
		return numOfMachines;
	}

	public int getInitialSeed() {
		return initialSeed;
	}

	public int getLowerBound() {
		return lowerBound;
	}

	public int getUpperBound() {
		return upperBound;
	}
}
