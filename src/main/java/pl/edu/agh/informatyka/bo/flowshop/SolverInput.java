package pl.edu.agh.informatyka.bo.flowshop;

import pl.edu.agh.informatyka.bo.flowshop.util.TestInstance;

import java.util.HashMap;
import java.util.Map;

public class SolverInput implements ISolverInput {

	private Map<String, String> data = new HashMap<>();
	private TestInstance testInstance = null;

	@Override
	public String getParam(String name) {
		return data.get(name);
	}

	@Override
	public void setParam(String name, String value) {
		data.put(name, value);
	}

	@Override
	public TestInstance getTestInstance() {
		return testInstance;
	}

	@Override
	public void setTestInstance(TestInstance instance) {
		this.testInstance = instance;
	}
}
