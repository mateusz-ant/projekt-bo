package pl.edu.agh.informatyka.bo.flowshop;

import java.util.List;

public class SolverOutput implements ISolverOutput {

	List<Integer> order;
	int makespan;
	int iterations;

	public List<Integer> getOrder() {
		return order;
	}

	public void setOrder(List<Integer> order) {
		this.order = order;
	}

	public int getMakespan() {
		return makespan;
	}

	public void setMakespan(int makespan) {
		this.makespan = makespan;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}
}
